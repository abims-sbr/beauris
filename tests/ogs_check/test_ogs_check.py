import os

from . import OgsCheckTestCase


class TestOgsCheck(OgsCheckTestCase):

    def test_work_dir(self, ogs_check, temp_dir):

        test_data_dir = "test-data/data/"

        out_gff = os.path.join(temp_dir, 'out.gff')
        ogs_check.check(test_data_dir + "test.gff", test_data_dir + "genome.fa", out_gff)

        expected = test_data_dir + "expected.gff"

        with open(expected, 'r') as exp:
            with open(out_gff, 'r') as gff:
                assert exp.read() == gff.read()

    def test_rna_prefix(self, ogs_check_rna_prefix, temp_dir):

        test_data_dir = "test-data/data/"

        out_gff = os.path.join(temp_dir, "out_rna_prefix.gff")
        ogs_check_rna_prefix.check(test_data_dir + "test_rna_prefix.gff", test_data_dir + "genome.fa", out_gff)

        expected = test_data_dir + "expected_rna_prefix.gff"

        with open(expected, "r") as exp:
            with open(out_gff, "r") as out:
                assert exp.read() == out.read()
