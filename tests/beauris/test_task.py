import os

from beauris.task import Task
from beauris.util import Util, find_mr_labels

from . import BeaurisTestCase


class TestTask(BeaurisTestCase):

    def test_label_force(self, org):

        tmp_task = Task(org, 'one_task')

        assert not tmp_task.needs_to_run()

        os.environ['CI_MERGE_REQUEST_LABELS'] = 'run-one_task'
        Util.mr_labels = find_mr_labels()

        assert tmp_task.needs_to_run()

        os.environ['CI_MERGE_REQUEST_LABELS'] = 'run-everything'
        Util.mr_labels = find_mr_labels()

        assert tmp_task.needs_to_run()

        os.environ['CI_MERGE_REQUEST_LABELS'] = 'run-one_task_xx'
        Util.mr_labels = find_mr_labels()

        assert not tmp_task.needs_to_run()

        os.environ['CI_MERGE_REQUEST_LABELS'] = 'run-one_task,disable-one_task'
        Util.mr_labels = find_mr_labels()

        assert not tmp_task.needs_to_run()

    def test_task_dep_nolock(self, org):

        tmp_task = Task(org, 'apollo_staging')

        assert not tmp_task.needs_to_run()

        deps = [org.assemblies[0].derived_files['jbrowse']]
        tmp_task.depends_on = deps

        assert tmp_task.needs_to_run()

    def test_task_dep(self, org_small_locked, locker):

        tmp_task = Task(org_small_locked, 'apollo_staging')

        # We need to make sure the locked path exists on the disk to reflect real situation
        org_small_locked.assemblies[0].derived_files['jbrowse'].locked_path = org_small_locked.assemblies[0].derived_files['jbrowse'].locked_path.replace('$LOCKED_DIR', locker.target_dir)
        os.makedirs(os.path.dirname(org_small_locked.assemblies[0].derived_files['jbrowse'].locked_path))
        open(org_small_locked.assemblies[0].derived_files['jbrowse'].locked_path, 'a').close()

        # No change in locked jbrowse => no need to run anything
        assert not org_small_locked.assemblies[0].derived_files['jbrowse'].task.needs_to_run()

        # No change in tmptask deps => no need to run anything
        assert not tmp_task.needs_to_run()

        assert org_small_locked.assemblies[0].derived_files['jbrowse'].get_revision() == 0

        # Simulate a dependency on jbrowse output + force run of the jbrowse task
        deps = [org_small_locked.assemblies[0].derived_files['jbrowse']]
        tmp_task.depends_on = deps

        os.environ['CI_MERGE_REQUEST_LABELS'] = 'run-jbrowse'
        Util.mr_labels = find_mr_labels()

        # Jbrowse needs to run now
        assert org_small_locked.assemblies[0].derived_files['jbrowse'].task.needs_to_run()

        # And the dependent task too
        assert tmp_task.needs_to_run()

        assert org_small_locked.assemblies[0].derived_files['jbrowse'].get_revision() == 1

    def test_task_deps(self, org):

        tmp_task = Task(org, 'one_task')

        assert not tmp_task.needs_to_run()

        tmp_task.depends_on = [org.assemblies[0].derived_files['jbrowse']]

        assert tmp_task.needs_to_run()
