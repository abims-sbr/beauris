import collections
import os

from beauris.blastbank import BankWriter

import yaml

from . import BeaurisTestCase


class TestBlastBank(BeaurisTestCase):

    def test_yml(self, org, tmp_dir):

        banks = org.get_blast_banks()

        writer = BankWriter(banks, tmp_dir, "staging")
        writer.write_bank_yml()
        writer.write_links_yml()

        expected_banks = {
            "genouest_blast": {
                "db_provider": {
                    "list": {
                        "nucleic": collections.OrderedDict({
                            os.path.splitext(org.assemblies[0].get_derived_path('blastdb_nhr'))[0]: "Blabla blobloblo GOGEPP5 assembly 10",
                            os.path.splitext(org.assemblies[0].annotations[0].get_derived_path('blastdb_cds_nhr'))[0]: "Blabla blobloblo GOGEPP5 assembly 10 annotation 1.5 CDS",
                            os.path.splitext(org.assemblies[0].annotations[0].get_derived_path('blastdb_transcripts_nhr'))[0]: "Blabla blobloblo GOGEPP5 assembly 10 annotation 1.5 transcripts",
                            os.path.splitext(org.assemblies[1].get_derived_path('blastdb_nhr'))[0]: "Blabla blobloblo GOGEPP5 assembly 4.1",
                        }),
                        "proteic": {
                            os.path.splitext(org.assemblies[0].annotations[0].get_derived_path('blastdb_proteins_phr'))[0]: "Blabla blobloblo GOGEPP5 assembly 10 annotation 1.5 proteins",
                        },
                    }
                }
            }
        }

        with open(tmp_dir / 'banks.yml') as outfile:
            banks_yml = yaml.safe_load(outfile.read())

            # Check banks order too
            assert collections.OrderedDict(banks_yml['genouest_blast']['db_provider']['list']['nucleic']) == expected_banks['genouest_blast']['db_provider']['list']['nucleic']
            assert collections.OrderedDict(banks_yml['genouest_blast']['db_provider']['list']['proteic']) == expected_banks['genouest_blast']['db_provider']['list']['proteic']

        expected_links = {
            "brassica_napus_ass10": {
                "db": "^brassica_napus_ass10$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass10&loc={id}{jbrowse_track}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%2010&loc={id}{apollo_track}">Apollo</a>',
            },
            "brassica_napus_ass10_annot1.5_CDS": {
                "db": "^brassica_napus_ass10_annot1.5_CDS$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass10&loc={id}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%2010&loc={id}">Apollo</a>',
            },
            "brassica_napus_ass10_annot1.5_proteins": {
                "db": "^brassica_napus_ass10_annot1.5_proteins$",
                "*": '{id}',
            },
            "brassica_napus_ass10_annot1.5_transcripts": {
                "db": "^brassica_napus_ass10_annot1.5_transcripts$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass10&loc={id}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%2010&loc={id}">Apollo</a>',
            },
            "brassica_napus_ass4.1": {
                "db": "^brassica_napus_ass4.1$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass4.1&loc={id}{jbrowse_track}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%204.1&loc={id}{apollo_track}">Apollo</a>',
            },
        }

        with open(tmp_dir / 'links.yml') as outfile:
            links_yml = yaml.safe_load(outfile.read())
            assert links_yml == expected_links

    def test_yml_locked(self, org_locked, tmp_dir, locker):
        # Same as test_yml, but with a locked organism

        org = org_locked

        self.create_fake_derived_files(org, locker.target_dir)

        banks = org.get_blast_banks()

        writer = BankWriter(banks, tmp_dir, "staging")
        writer.write_bank_yml()
        writer.write_links_yml()

        expected_banks = {
            "genouest_blast": {
                "db_provider": {
                    "list": {
                        "nucleic": collections.OrderedDict({
                            os.path.splitext(org.assemblies[0].derived_files['blastdb_nhr'].get_locked_path())[0]: "Blabla blobloblo GOGEPP5 assembly 10",
                            os.path.splitext(org.assemblies[0].annotations[0].derived_files['blastdb_cds_nhr'].get_locked_path())[0]: "Blabla blobloblo GOGEPP5 assembly 10 annotation 1.5 CDS",
                            os.path.splitext(org.assemblies[0].annotations[0].derived_files['blastdb_transcripts_nhr'].get_locked_path())[0]: "Blabla blobloblo GOGEPP5 assembly 10 annotation 1.5 transcripts",
                            os.path.splitext(org.assemblies[1].derived_files['blastdb_nhr'].get_locked_path())[0]: "Blabla blobloblo GOGEPP5 assembly 4.1",
                        }),
                        "proteic": {
                            os.path.splitext(org.assemblies[0].annotations[0].derived_files['blastdb_proteins_phr'].get_locked_path())[0]: "Blabla blobloblo GOGEPP5 assembly 10 annotation 1.5 proteins",
                        },
                    }
                }
            }
        }

        with open(tmp_dir / 'banks.yml') as outfile:
            banks_yml = yaml.safe_load(outfile.read())

            # Check banks order too
            assert collections.OrderedDict(banks_yml['genouest_blast']['db_provider']['list']['nucleic']) == expected_banks['genouest_blast']['db_provider']['list']['nucleic']
            assert collections.OrderedDict(banks_yml['genouest_blast']['db_provider']['list']['proteic']) == expected_banks['genouest_blast']['db_provider']['list']['proteic']

        expected_links = {
            "brassica_napus_ass10": {
                "db": "^brassica_napus_ass10$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass10&loc={id}{jbrowse_track}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%2010&loc={id}{apollo_track}">Apollo</a>',
            },
            "brassica_napus_ass10_annot1.5_CDS": {
                "db": "^brassica_napus_ass10_annot1.5_CDS$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass10&loc={id}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%2010&loc={id}">Apollo</a>',
            },
            "brassica_napus_ass10_annot1.5_proteins": {
                "db": "^brassica_napus_ass10_annot1.5_proteins$",
                "*": '{id}',
            },
            "brassica_napus_ass10_annot1.5_transcripts": {
                "db": "^brassica_napus_ass10_annot1.5_transcripts$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass10&loc={id}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%2010&loc={id}">Apollo</a>',
            },
            "brassica_napus_ass4.1": {
                "db": "^brassica_napus_ass4.1$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass4.1&loc={id}{jbrowse_track}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%204.1&loc={id}{apollo_track}">Apollo</a>',
            },
        }

        with open(tmp_dir / 'links.yml') as outfile:
            links_yml = yaml.safe_load(outfile.read())
            assert links_yml == expected_links

    def test_yml_locked_modified(self, org_locked, tmp_dir, locker):
        # Same as test_yml, but with a locked organism

        org = org_locked

        self.create_fake_derived_files(org, locker.target_dir)

        banks = org.get_blast_banks()

        # Simulate a change in a previously locked gff
        org.assemblies[0].annotations[0].input_files['gff'].revision = 2

        writer = BankWriter(banks, tmp_dir, "staging")
        writer.write_bank_yml()
        writer.write_links_yml()

        expected_banks = {
            "genouest_blast": {
                "db_provider": {
                    "list": {
                        "nucleic": collections.OrderedDict({
                            os.path.splitext(org.assemblies[0].derived_files['blastdb_nhr'].get_locked_path())[0]: "Blabla blobloblo GOGEPP5 assembly 10",
                            os.path.splitext(org.assemblies[0].annotations[0].derived_files['blastdb_cds_nhr'].path)[0]: "Blabla blobloblo GOGEPP5 assembly 10 annotation 1.5 CDS",
                            os.path.splitext(org.assemblies[0].annotations[0].derived_files['blastdb_transcripts_nhr'].path)[0]: "Blabla blobloblo GOGEPP5 assembly 10 annotation 1.5 transcripts",
                            os.path.splitext(org.assemblies[1].derived_files['blastdb_nhr'].get_locked_path())[0]: "Blabla blobloblo GOGEPP5 assembly 4.1",
                        }),
                        "proteic": {
                            os.path.splitext(org.assemblies[0].annotations[0].derived_files['blastdb_proteins_phr'].path)[0]: "Blabla blobloblo GOGEPP5 assembly 10 annotation 1.5 proteins",
                        },
                    }
                }
            }
        }

        with open(tmp_dir / 'banks.yml') as outfile:
            banks_yml = yaml.safe_load(outfile.read())

            # Check banks order too
            assert collections.OrderedDict(banks_yml['genouest_blast']['db_provider']['list']['nucleic']) == expected_banks['genouest_blast']['db_provider']['list']['nucleic']
            assert collections.OrderedDict(banks_yml['genouest_blast']['db_provider']['list']['proteic']) == expected_banks['genouest_blast']['db_provider']['list']['proteic']

        expected_links = {
            "brassica_napus_ass10": {
                "db": "^brassica_napus_ass10$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass10&loc={id}{jbrowse_track}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%2010&loc={id}{apollo_track}">Apollo</a>',
            },
            "brassica_napus_ass10_annot1.5_CDS": {
                "db": "^brassica_napus_ass10_annot1.5_CDS$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass10&loc={id}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%2010&loc={id}">Apollo</a>',
            },
            "brassica_napus_ass10_annot1.5_proteins": {
                "db": "^brassica_napus_ass10_annot1.5_proteins$",
                "*": '{id}',
            },
            "brassica_napus_ass10_annot1.5_transcripts": {
                "db": "^brassica_napus_ass10_annot1.5_transcripts$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass10&loc={id}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%2010&loc={id}">Apollo</a>',
            },
            "brassica_napus_ass4.1": {
                "db": "^brassica_napus_ass4.1$",
                "*": '<a href="https://staging.example.org/sp/brassica_napus/jbrowse/?data=data%2Fbrassica_napus_ass4.1&loc={id}{jbrowse_track}">{id}</a> <a href="http://staging.example.apollo.bipaa.org/apollo-staging/annotator/loadLink?organism=Blabla%20blobloblo%20GOGEPP5%204.1&loc={id}{apollo_track}">Apollo</a>',
            },
        }

        with open(tmp_dir / 'links.yml') as outfile:
            links_yml = yaml.safe_load(outfile.read())
            assert links_yml == expected_links
