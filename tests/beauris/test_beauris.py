from . import BeaurisTestCase


class TestBeauris(BeaurisTestCase):

    def test_services(self, beauris):

        assert beauris.config.get_deploy_services("staging") == [
            "blast",
            "download",
            "jbrowse",
            "authelia",
            "apollo",
        ]
