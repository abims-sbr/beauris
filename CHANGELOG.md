# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- Documentation on [https://beauris.readthedocs.io](https://beauris.readthedocs.io/)
- Genoboo integration

### Changed

- Fixed Orson execution (!3)
- Reworked Apollo permissions update (!1)
- Fix interfaces bugs (#51 and #43)
- Fix execution of DRMAA tasks
- Add support for DNASeq bams
- Change bam_to_wig method (faster, uses less memory)

## [0.1] - 2023-03-31

### Added

- First ever release of Beauris
