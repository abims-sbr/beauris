# BEAURIS modules

Several modules are implemented in BEAURIS, you can select the ones you're interested in by configuring it in beauris.yml

## TODO list all implemented modules

## Functional annotation with the Nextflow pipeline Orson

The `func_annot_orson` task runs:
- busco (coming)
- beedeem
- diamond on uniref90
- interproscan
- eggnogmapper

Nextflow handles job submissions on SLURM (or other environments) and supervises the jobs.

### Requirements

Requires singularity, nextflow, graphviz.

Download Orson code and singularity images (put them into `orson/containers`)
```bash
git clone https://gitlab.ifremer.fr/bioinfo/workflows/orson.git
wget -r -nc -l1 -nH --cut-dirs=6 -A '*.sif' ftp://ftp.ifremer.fr/ifremer/dataref/bioinfo/sebimer/tools/ORSON/ -P orson/containers
```

Download the nextflow config file for your cluster, e.g.:
```bash
wget https://github.com/nf-core/configs/blob/master/conf/abims.config
```

You also need the uniref90 blast bank, indexed with diamond.

### Usage

- Set the CI step `func_annot` to use `beauris.workflows.drmaa.func_annot_orson`
- Set `job_specs` as follows in `beauris.yml`:

```yaml
job_specs:
  drmaa:
    func_annot_orson:
      # SCRATCH_WORK_DIR is supposed to be a scratch storage area for nextflow that can be cleaned up once the computation is completed.
      env : >
        source /etc/profile.d/modules.sh; module load nextflow/22.10.0 graphviz/2.40.1;
        export ORSON_PATH=/path/to/orson/;
        export BLAST_DB_PATH=/path/to/uniref90/diamond/uniref90_2022_03/uniref90.dmnd;
        export CLUSTER_CONFIG_PATH=/path/to/cluster.config;
        export SCRATCH_WORK_DIR=/path/to/scratch/workdir
```

### Options
HECTAR (for heterokonts) is activated when it is defined as a task option in the yaml of the organism:
```yaml
    annotations:
      - version: "OGS1.0"
        [...]
        task_options:
          func_annot_orson:
            hectar: true
```

Note: HECTAR can only be used on the ABIMS cluster for now because the source code is still not published.
